import { Routes } from '@angular/router';

export class RouteConfig {
  static ROUTES: Routes = [
    { path: '', redirectTo: 'books', pathMatch: 'full' },
    {
      path: 'books',
      loadChildren: () =>
        import('../components/books/books.module').then(
          (mod) => mod.BooksModule
        ),
    },
  ];
}
