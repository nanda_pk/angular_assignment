import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Book, BookVolume } from '../shared/shared-models';

@Injectable({
  providedIn: 'root',
})
export class BooksDataService {
  private readonly AUTH_KEY = 'AIzaSyB-tLAK0Wp8mRjGHOr1Iq9FuWEym6R6Y3o';
  private readonly BASE_URL = 'https://www.googleapis.com/books/v1/volumes?';
  constructor(private httpClient: HttpClient) {}

  getAllBooks(): Observable<Book[]> {
    const SEARCH_TERM = null;
    const URL = `${this.BASE_URL}q=${SEARCH_TERM}&key=${this.AUTH_KEY}`;
    return this.httpClient
      .get<BookVolume>(URL)
      .pipe(map((res) => res?.items || []));
  }
  getBooksByName(searchKey: string): Observable<Book[]> {
    const URL = `${this.BASE_URL}q=${searchKey}&key=${this.AUTH_KEY}`;
    return this.httpClient
      .get<BookVolume>(URL)
      .pipe(map((res) => res?.items || []));
  }
}
