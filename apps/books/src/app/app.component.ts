import { Component } from '@angular/core';

@Component({
  selector: 'book-purchase-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'books';
}
