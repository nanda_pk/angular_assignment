interface VolumeInfo {
  title: string;
  subtitle: string;
  imageLinks: ImageInfo;
  authors: string[];
}

interface ImageInfo {
  smallThumbnail: string;
}

export interface Book {
  volumeInfo: VolumeInfo;
  publisher: string;
  publishedDate: string;
  description: string;
  pageCount: number;
  maturityRating: string;
  language: string;
}
export interface BookVolume {
  items: Book[];
  kind: string;
  totalItems: number;
}
