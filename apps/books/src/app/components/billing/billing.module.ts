import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyBookFormComponent } from './buy-book-form/buy-book-form.component';

@NgModule({
  declarations: [BuyBookFormComponent],
  imports: [CommonModule],
})
export class BillingModule {}
