import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookListComponent } from './book-list/book-list.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [BookListComponent],
  imports: [CommonModule, MaterialModule],
  exports: [BookListComponent, MaterialModule],
})
export class SharedModule {}
