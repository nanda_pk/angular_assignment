import { Component, Input } from '@angular/core';
import { Book } from '../../../shared/shared-models';

@Component({
  selector: 'book-purchase-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss'],
})
export class BookListComponent {
  @Input() books: Book[] = [];
}
