import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookContainerComponent } from './book-container/book-container.component';
import { BookViewComponent } from './book-view/book-view.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: BookContainerComponent },
  { path: 'view', component: BookViewComponent },
];
@NgModule({
  declarations: [BookContainerComponent, BookViewComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTES)],
})
export class BooksModule {}
