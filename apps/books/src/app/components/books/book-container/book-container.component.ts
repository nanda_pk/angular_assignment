import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { BooksDataService } from '../../../services/books-data.service';
import { Book } from '../../../shared/shared-models';

@Component({
  selector: 'book-purchase-book-container',
  templateUrl: './book-container.component.html',
  styleUrls: ['./book-container.component.scss'],
})
export class BookContainerComponent implements OnInit, AfterViewInit {
  @ViewChild('searchRef') searchElement: ElementRef = new ElementRef(null);
  books: Book[] = [];
  constructor(private booksDataService: BooksDataService) {}

  ngOnInit(): void {
    this.booksDataService.getAllBooks().subscribe(
      (books: Book[]) => this.onFetchBooks(books),
      (error: ErrorEvent) => this.notifyError(error)
    );
  }

  ngAfterViewInit() {
    fromEvent(this.searchElement.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(() => {
        this.booksDataService
          .getBooksByName(this.searchElement?.nativeElement?.value)
          .subscribe(
            (books) => this.onFetchBooks(books),
            (error: ErrorEvent) => this.notifyError(error)
          );
      });
  }
  private onFetchBooks(books: Book[]) {
    this.books = books;
  }
  private notifyError(error: ErrorEvent) {
    console.error(error?.error?.message);
  }
}
