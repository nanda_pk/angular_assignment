import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookCollectionListComponent } from './book-collection-list/book-collection-list.component';



@NgModule({
  declarations: [
    BookCollectionListComponent
  ],
  imports: [
    CommonModule
  ]
})
export class BookCollectionModule { }
