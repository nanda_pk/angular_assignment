import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCollectionListComponent } from './book-collection-list.component';

describe('BookCollectionListComponent', () => {
  let component: BookCollectionListComponent;
  let fixture: ComponentFixture<BookCollectionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookCollectionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCollectionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
